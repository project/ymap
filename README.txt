This is the start of a Yahoo Maps module. It requires the location module to 
function.  Refer to the INSTALL.txt for information on how to enable and
configure this module.

Currently supports the display of node location on a Yahoo! map.  The map
can render both lat/long location information and also supports address
mapping based on the built in geocoding functions of the Yahoo! map API.

Map settings can be enabled and configured per node type and are themeable.  
Most of the available Yahoo! map controls can be configured.

A default YMap view is also supplied to allow the display of multiple nodes
on a single map with the display of themable expandable markers.  

Near term development will add support for more map types other than AJAX
(e.g. Flash), and support for the overlay of GeoRSS feeds.

This has been tested with Drupal 4.7